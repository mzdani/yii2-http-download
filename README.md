Yii2 HTTP Download
=============

[![Latest Stable Version](https://poser.pugx.org/mdscomp/yii2-http-download/v/stable)](https://packagist.org/packages/mdscomp/yii2-http-download) [![Total Downloads](https://poser.pugx.org/mdscomp/yii2-http-download/downloads)](https://packagist.org/packages/mdscomp/yii2-http-download) [![Latest Unstable Version](https://poser.pugx.org/mdscomp/yii2-http-download/v/unstable)](https://packagist.org/packages/mdscomp/yii2-http-download) [![License](https://poser.pugx.org/mdscomp/yii2-http-download/license)](https://packagist.org/packages/mdscomp/yii2-http-download)

This class will execute file download with HTTP port. 

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist "mdscomp/yii2-http-download" "~1.0"
```

or add

```
"mdscomp/yii2-http-download": "~1.0"
```

to the require section of your `composer.json` file.


Usage
-----